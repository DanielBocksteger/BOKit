//
//  BOKit.h
//  BOKit
//
//  Created by Daniel Bocksteger on 14.10.17.
//  Copyright © 2017 Daniel Bocksteger. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BOKit.
FOUNDATION_EXPORT double BOKitVersionNumber;

//! Project version string for BOKit.
FOUNDATION_EXPORT const unsigned char BOKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BOKit/PublicHeader.h>


