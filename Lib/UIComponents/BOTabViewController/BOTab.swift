//
//  BOTab.swift
//  BOKit
//
//  Created by Daniel Bocksteger on 14.10.17.
//  Copyright © 2017 Daniel Bocksteger. All rights reserved.
//

import Foundation
import UIKit

public typealias BOTabPresentablePredicate = ((Any) -> Bool)
public typealias BOTabPresentablePresentationHandle = ((BOTab) -> Void)
public typealias BOTabPresentableSouldPresentHandle = ((BOTab) -> (Bool, String))

@objc protocol BOTabPresentable {
    var predicate: BOTabPresentablePredicate { get set }
    var presentationHandle: BOTabPresentablePresentationHandle? { get set }
    
    func isPresentable(context: Any) -> Bool
}

@objc public protocol BOTabContentViewable {
    
    func gotVisible(_ state: Bool, in tabViewController: BOTabViewController)
    
    func refresh()
}

@objc public class BOTab: NSObject, BOTabPresentable {
    
    public var tab: String
    public var title: String
    public var viewController: UIViewController
    
    var predicate: BOTabPresentablePredicate
    
    var shouldPresentHandle: BOTabPresentableSouldPresentHandle?
    
    var presentationHandle: BOTabPresentablePresentationHandle?
    
    @objc public init(tab: String, title: String, viewController: UIViewController, predicate: @escaping BOTabPresentablePredicate) {
        self.tab = tab
        self.title = title
        self.viewController = viewController
        
        self.predicate = predicate
    }
    
    @objc public init(tab: String, title: String, viewController: UIViewController, predicate: @escaping BOTabPresentablePredicate, presentationHandle: BOTabPresentablePresentationHandle?) {
        self.tab = tab
        self.title = title
        self.viewController = viewController
        
        self.predicate = predicate
        
        self.presentationHandle = presentationHandle
    }
    
    public init(tab: String, title: String, viewController: UIViewController, predicate: @escaping BOTabPresentablePredicate, presentationHandle: BOTabPresentablePresentationHandle?, shouldPresentHandle: BOTabPresentableSouldPresentHandle?) {
        self.tab = tab
        self.title = title
        self.viewController = viewController
        
        self.predicate = predicate
        
        self.shouldPresentHandle = shouldPresentHandle
        
        self.presentationHandle = presentationHandle
    }
    
    func isPresentable(context: Any) -> Bool {
        return predicate(context)
    }
}
