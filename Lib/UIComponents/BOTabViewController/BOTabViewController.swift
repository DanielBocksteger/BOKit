//
//  BOTabViewController.swift
//  BOKit
//
//  Created by Daniel Bocksteger on 14.10.17.
//  Copyright © 2017 Daniel Bocksteger. All rights reserved.
//

import UIKit

@objc public enum BOTablViewControllerPresentationStyle: Int {
    case none
    case bottomBar
    case topBar /* unsupported */
}

@objc open class BOTabViewController: UIViewController {

    // -- Public members
    
    public var leftTopBarItems: [UIBarButtonItem]? {
        didSet {
            self.buildToolbars()
        }
    }
    
    public var rightTopBarItems: [UIBarButtonItem]? {
        didSet {
            self.buildToolbars()
        }
    }
    
    public var leftBottomBarItems: [UIBarButtonItem]? {
        didSet {
            self.buildToolbars()
        }
    }
    
    public var rightBottomBarItems: [UIBarButtonItem]? {
        didSet {
            self.buildToolbars()
        }
    }
    
    public var context: Any? {
        didSet {
            self.buildToolbars()
            
            // Notify the current tab about the context changes
            if let tab = selectedTab, let viewable = tab.viewController as? BOTabContentViewable {
                viewable.gotVisible(true, in: self);
            }
        }
    }
    
    public var selectedTab: BOTab? {
        didSet {
            guard let tab = selectedTab else { return }
            
            self.title = tab.title
            
            self.add(content: tab.viewController)
        }
    }
    
    public var contextWorker: ((UIViewController) -> Any?)?
    
    // -- Private members
    
    private var forceSelectIndex: Int = 0
    
    private var tabs: [BOTab]?
    private var lastSelectionIndex: Int?
    
    private var tabControl: UISegmentedControl?
    private var tabControlBBI: UIBarButtonItem?
    
    private var tabContentView: UIView?
    
    @objc public init(style: BOTablViewControllerPresentationStyle, tabs: [BOTab], selectedIndex: Int, context: Any?) {
        self.context = context
        
        print("[BOTabVC] - Initialized with context: \(String(describing: context))")
        
        super.init(nibName: nil, bundle: nil)
        
        self.tabs = tabs
        self.forceSelectIndex = selectedIndex
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        self.navigationController?.isToolbarHidden = false
        
        buildToolbars()
    }

    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let tab = selectedTab, let viewable = tab.viewController as? BOTabContentViewable {
            viewable.gotVisible(true, in: self);
        }
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func buildToolbars() {
        // Top Bar
        self.navigationItem.leftBarButtonItems = leftTopBarItems
        self.navigationItem.rightBarButtonItems = rightTopBarItems
        
        // Bottom Bar
        var items = [UIBarButtonItem]()
        
        if let left = leftBottomBarItems {
            items.append(contentsOf: left)
        }
        
        setupTabControl()
        
        if let tabber = tabControlBBI {
            items.append(tabber)
        }
        
        if let right = rightBottomBarItems {
            items.append(contentsOf: right)
        }
        
        self.toolbarItems = items
    }
    
    public func refresh() {
        if let tabContent = selectedTab?.viewController as? BOTabContentViewable {
            tabContent.refresh()
        }
    }
}

// Mark: Segment control stuff

extension BOTabViewController {
    
    private var usableTabs: [BOTab] {
        var result = [BOTab]()
        
        guard let tabList = tabs else { return result }
        
        for tab in tabList {
            if let con = context, tab.isPresentable(context: con) == true {
                result.append(tab)
            }
        }
        
        return result
    }
    
    private var tabItems: [String]? {
        var items = [String]()
        for tab in usableTabs {
            items.append(tab.tab)
        }
        
        return items
    }
    
    private func setupTabControl() {
        let tabber = UISegmentedControl(items: tabItems)
        
        let selectionChanged = lastSelectionIndex != forceSelectIndex
        
        tabber.addTarget(self, action: #selector(onCurrentTabChanged), for: .valueChanged)
        tabber.selectedSegmentIndex = forceSelectIndex
        
        tabControl = tabber
        tabControlBBI = UIBarButtonItem(customView: tabber)
        
        if selectionChanged == true {
            onCurrentTabChanged()
        }
    }
    
    @objc private func onCurrentTabChanged() {
        guard let control = tabControl else { return }
        
        let tabList = usableTabs
        
        let recentIndex = lastSelectionIndex ?? 0
        
        var index: Int = control.selectedSegmentIndex
        if index < 0 {
            index = 0
        }
        
        lastSelectionIndex = index
        forceSelectIndex = index
        
        if tabList.count > index {
            var shouldPresent = true
            var errorMessage = ""
            
            if lastSelectionIndex ?? 0 >= 0 {
                if let handle = tabList[index].shouldPresentHandle {
                    (shouldPresent, errorMessage) = handle(tabList[index])
                }
            }
            
            if shouldPresent {
                // Inform the previous content, that it got invisible
                if let tab = selectedTab, let viewable = tab.viewController as? BOTabContentViewable {
                    viewable.gotVisible(false, in: self);
                    
                    rightTopBarItems = []
                    
                    leftBottomBarItems = []
                    rightBottomBarItems = []
                }

                selectedTab = tabList[index]
                
                // Inform the content, that it got visible
                if let tab = selectedTab, let viewable = tab.viewController as? BOTabContentViewable {
                    viewable.gotVisible(true, in: self);
                    
                    if let handle = tab.presentationHandle {
                        handle(tab)
                    }
                }
            } else {
                let alert = UIAlertController(title: "Hinweis", message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [unowned self] (_) in
                    self.tabControl?.selectedSegmentIndex = recentIndex >= 0 ? recentIndex : 0
                    
                    self.onCurrentTabChanged()
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

// Mark: Extend the tab view controller with a comfortable way to add child view controllers

extension BOTabViewController {

    func add(content: UIViewController) {
        if let prevContentView = tabContentView {
            prevContentView.removeFromSuperview()
        }
        
        addChildViewController(content)
        
        tabContentView = content.view
        
        view.addSubview(content.view)
        
        content.view.translatesAutoresizingMaskIntoConstraints = false

        let leading = NSLayoutConstraint(item: content.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0.0)
        let trailing = NSLayoutConstraint(item: content.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0.0)
        
        content.view.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor).isActive = true
        content.view.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor).isActive = true

        leading.isActive = true
        trailing.isActive = true
        
        content.didMove(toParentViewController: self)
    }
}

// Mark: Extend the tab view controller to allow tab-selection from outside the box

extension BOTabViewController {
    
    public func doSelect(at index: Int) {
        tabControl?.selectedSegmentIndex = index
        
        self.onCurrentTabChanged()
    }
}
