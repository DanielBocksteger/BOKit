//
//  ViewController.swift
//  BOTabApp
//
//  Created by Daniel Bocksteger on 14.10.17.
//  Copyright © 2017 Daniel Bocksteger. All rights reserved.
//

import UIKit
import BOKit

struct BOTabContext {
    var name: String
}

class ViewController: UIViewController {

    let context = BOTabContext(name: "Test")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func pushedIt(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let tabs = [
            BOTab(tab: "Yellow", title: "A yellow View", viewController: storyboard.instantiateViewController(withIdentifier: "yellow"), predicate: { (context: Any) in
                if let tabContext = context as? BOTabContext, tabContext.name == "Test" {
                    return true
                }
                return false
            }),
            BOTab(tab: "Red", viewController: storyboard.instantiateViewController(withIdentifier: "red"), predicate: { (context: Any) in
                if let tabContext = context as? BOTabContext, tabContext.name == "Test" {
                    return true
                }
                return false
            }),
            BOTab(tab: "Green", title: "A green View", viewController: storyboard.instantiateViewController(withIdentifier: "green"), predicate: { (context: Any) in
                if let tabContext = context as? BOTabContext, tabContext.name == "Test" {
                    return true
                }
                return false
            }),
            BOTab(tab: "Blue", title: "A blue View", viewController: storyboard.instantiateViewController(withIdentifier: "blue"), predicate: { (context: Any) in
                if let tabContext = context as? BOTabContext, tabContext.name == "Test" {
                    return true
                }
                return false
            })
        ]
        
        let tabVC = BOTabViewController(style: .bottomBar, tabs: tabs, selectedIndex: 2, context: context)
        
        self.navigationController?.pushViewController(tabVC, animated: true)
    }
}
